#!/usr/bin/env python3
import sys
import os
import csv

if __name__ == "__main__":

        
	# importing csv with splunk versions

	file = open('versions.csv')	
	reader = csv.reader(file)
	pos = list(reader)
	verificado = 0

	file = open('fversions.csv')	
	freader = csv.reader(file)
	fpos = list(freader)

	#Verifying version exists
	while(verificado==0):
            if sys.argv[2] == "-f":
                print("=================================================================================")
                for i in range(1,176):
                    if pos[i][0] == sys.argv[1]:
                            os.system("clear")
                            print("-------------------------------------")
                            print('Downloading Splunk Forwarder ' + pos[i][0])
                            print("-------------------------------------")
                            version = fpos[i][0] 
                            name = fpos[i][1] 
                            os.system( "wget -O " + name + " 'https://www.splunk.com/page/download_track?file=" + version + "/linux/" + name + "&ac=&wget=true&name=wget&platform=Linux&architecture=x86_64&version=" + version+ "&product=universalforwarder&typed=release'")
                            verificado = 1
        


            else:
                for i in range(1,176):
                    if pos[i][0] == sys.argv[1]:
                            os.system("clear")
                            print("-------------------------------------")
                            print('Downloading Splunk ' + pos[i][0])
                            print("-------------------------------------")
                            version = pos[i][0] 
                            name = pos[i][1] 
                            os.system( "wget -O " + name + " 'https://www.splunk.com/page/download_track?file=" + version + "/linux/" + name + "&ac=&wget=true&name=wget&platform=Linux&architecture=x86_64&version=" + version+ "&product=splunk&typed=release'")

                            verificado = 1
            

	
	print("-------------------------------------")
	print('Decompressing Splunk tar file')
	print("-------------------------------------")
 
	os.system("sudo tar xvfz splunk*.tgz -C /opt && sudo chown -R splunker:splunker /opt/splunk*")
	
	print("-------------------------------------")
	print('Starting Splunk')
	print("-------------------------------------")
	
	os.system("/opt/splunk*/bin/splunk start --accept-license --answer-yes --seed-passwd admin1234")
